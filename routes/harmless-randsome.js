var express = require('express');
var router = express.Router();

router.get('/', function (req, res, next) {
    /**
     * Binary search ==> dictionary research
     * Linear Time Algorithm Complexity ==> O(n + m)
     *
     * @param noteText
     * @param magicMagazine
     * @returns {string}
     */
    function harmlessRandsomeNote(noteText, magicMagazine) {
        const wordsNote = noteText.split(' ');
        const wordsMagic = magicMagazine.split(' ');
        let magObj = {};
        let noteIsPossible = true;

        wordsMagic.forEach((word) => {
            word.toLowerCase();
            if (!magObj[word]) {
                magObj[word] = 0;
            }
            magObj[word]++;
        });

        wordsNote.forEach((word) => {
            word.toLowerCase();
            if (magObj[word]) {
                magObj[word]--;
                if (magObj[word] < 0) {
                    noteIsPossible = false;
                }
            } else {
                noteIsPossible = false;
            }
        });

        return noteIsPossible ? 'Possible' : 'Not possible';
    }

    const response = harmlessRandsomeNote('This is my note', 'Magazine this note is a fucking hawesome newspaper my');

    res.render('answer', { arrayResponse: [], stringResponse: response });
});

module.exports = router;


