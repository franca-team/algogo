var express = require('express');
var router = express.Router();

router.get('/', function (req, res, next) {

    // array.sort((a, b) => a > b);
    /**
     * sort least to higher
     * @param array
     * @returns {*}
     */
    function bubbleSort(array) {

        for (let i = array.length - 1; i >= 0; i--) {
            for (let j = 0; j < i; j++) {
                if (array[j] > array[j + 1]) {
                    let temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
        return array;

    }

    function myBubbleSort(array) {
        for (let i = 0; i <= array.length; i++) {
            for (let j = 0; j < array.length; j++) {
                if (array[j] > array[j + 1]) {
                    let temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
    }

    const arrayResponse = bubbleSort([10, 5, 8, 9, 3, 2]);


    res.render('answer', { arrayResponse, stringResponse: '', objectResponse: {} });
});

module.exports = router;
