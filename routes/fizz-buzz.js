var express = require('express');
var router = express.Router();

router.get('/', function (req, res, next) {
    /**
     * Modulo test
     * @param num
     * @returns {[]}
     */
    function fizzBuzz(num) {
        let result = [];

        for (let i = 1; i <= num; i++) {

            // number divisable by 5 & 3
            if (!(i % 15)) {
                result.push('BuzzFizz');

                // number divisable by 3
            } else if (!(i % 3)) {
                result.push('Fizz');

                // number divisable by 5
            } else if (!(i % 5)) {
                result.push('Buzz');

            } else {
                result.push(i);
            }
        }
        return result;
    }

    const response = fizzBuzz(20);

    res.render('answer', { arrayResponse: response, stringResponse: '' });
});

module.exports = router;


