var express = require('express');
var router = express.Router();

router.get('/', function (req, res, next) {

    /**
     *
     * @param string
     * @param num
     * @returns {string}
     */
    function caesarCipher(string, num) {
        const lowerCase = string.toLowerCase();
        const alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('');
        let newString = '';

        // case num is higher or smaller than alphabet.length
        num = num % alphabet.length;

        for (let i = 0; i < lowerCase.length; i++) {
            let currentIndex = alphabet.indexOf(lowerCase[i]);
            let newIndex = currentIndex + num;

            if (lowerCase[i] === ' ') {
                newString += ' ';
            }

            if (newIndex >= alphabet.length) {
                newIndex = newIndex - alphabet.length;
            }

            if (newIndex < 0) {
                newIndex = alphabet.length + newIndex;
            }

            if (string[i] === string[i].toUpperCase()) {
                newString += alphabet[newIndex].toUpperCase();
            } else {
                newString += alphabet[newIndex];
            }
        }
        return newString;
    }

    let response = caesarCipher('Javascript', -900);
    // 'zoo keeper', 2 => bqq mggrgt
    // 'archer', -2 => ypafcp
    // 'pAlindrome', 26 => pAlindrome
    // 'Javascript', 900 => Tkfkcmbszd

    res.render('answer', { arrayResponse: [], stringResponse: response });
});

module.exports = router;
