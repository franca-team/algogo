var express = require('express');
var router = express.Router();

router.get('/', function (req, res, next) {

    function myReverseWords(string) {
        const myWords = string.split(' ');
        let result = '';

        myWords.forEach((word) => {
            result += ' ';
            for (let i = 0; i < word.length; i++) {
                result += word[(word.length - 1) - i];
            }
        });

        return result;
    }

    function reverseWords(string) {
        let wordsArr = string.split(' ');
        let reversedWordsArr = [];

        wordsArr.forEach(word => {
            let reversedWord = '';
            for (let i = word.length - 1; i >= 0; i--) {
                reversedWord += word[i];
            }
            reversedWordsArr.push(reversedWord);
        });

        return reversedWordsArr.join(' ');
    }

    function myReverseArray(array) {
        for (let i = array.length - 1; i >= 0; i--) {
            array.push(array[i]);
            array.splice(i, 1);
        }

        return array;
    }

    function reverseArrayInPlace(arr) {
        for (var i = 0; i < arr.length / 2; i++) {
            var tempVar = arr[i];
            arr[i] = arr[arr.length - 1 - i];
            arr[arr.length - 1 - i] = tempVar;
        }

        return arr;
    }

    let response = myReverseWords('this is a string of words');
    // this is a string of words => siht si gnirts fo sdrow

    let arrayResponse = myReverseArray([9, 8, 7, 6, 5]);

    res.render('answer', { arrayResponse, stringResponse: response });
});

module.exports = router;
