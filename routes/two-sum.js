var express = require('express');
var router = express.Router();

router.get('/', function (req, res, next) {
    function twoSum(numArray, sum) {
        let newArr = [];
        let pair = [];
        let i = 0;

        while (i < numArray.length) {
            const pairB = sum - numArray[i];

            if (numArray.indexOf(pairB) !== -1) {
                pair = [numArray[i], pairB];

                const currentIndex = numArray.indexOf(pairB);
                numArray.splice(currentIndex, 1);

                newArr.push(pair);
            }
            i++;
        }
        return newArr;
    }

    const arrayResponse = twoSum([1, 6, 4, 5, 3, 3, 0], 6);
    // [1, 6, 4, 5, 3, 3], 2 => [ [6, 1], [3, 4], [3, 4] ]

    res.render('answer', { arrayResponse, stringResponse: '', objectResponse: {} });
});

module.exports = router;
