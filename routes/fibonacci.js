var express = require('express');
var router = express.Router();

router.get('/', function (req, res, next) {

    // 1 1 2 3 5 8 13 21
    /**
     * Exponential function fibonacci
     * @param position
     * @returns {*}
     */
    function fibonacci(position) {
        let result = null;
        if (position <= 2) {
            result = 1;
        } else {
            result = fibonacci(position - 2) + fibonacci(position - 1);
        }
        return result;
    }

    /**
     * With memoization runtime linear O(n)
     * @param index
     * @param cache
     */
    function fibMemo(index, cache) {
        cache = cache || [];
        let result = null;

        if (cache[index]) {
            return cache[index];
        } else {
            if (index < 3) {
                result = 1;
            } else {
                cache[index] = fibMemo(index - 1, cache) + fibMemo(index - 2, cache);
                result = cache[index];
            }
        }
        return result;
    }

    // const stringResponse = fibonacci(9);
    // 4 => 3
    // 9 => 34
    const stringResponse = fibMemo(1000);
    // 12 => 144
    // 15 => 610


    res.render('answer', { arrayResponse: [], stringResponse, objectResponse: {} });
});

module.exports = router;
