var express = require('express');
var router = express.Router();

router.get('/', function (req, res, next) {

    function sieveOfEratosthenes(nb) {
        let result = [];
        let i = 2;

        while (i <= nb) {
            // numbers are prime by default
            let a = false;

            for (let j = 2; j < i; j++) {
                if (i % j === 0) {
                    // case not prime number
                    a = true;
                }
            }
            if (a === false) {
                result.push(i);
            }
            i++;
        }
        return result;
    }

    const arrayResponse = sieveOfEratosthenes(100);
    // 20 => [2, 3, 5, 7, 11, 13, 19]


    res.render('answer', { arrayResponse, stringResponse: '', objectResponse: {} });
});

module.exports = router;
