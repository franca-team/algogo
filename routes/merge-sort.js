var express = require('express');
var router = express.Router();

router.get('/', function (req, res, next) {

    /**
     *
     * @param arr
     * @returns {[]|*}
     */
    function mergeSort(arr) {
        if (arr.length < 2) {
            return arr;
        }

        // find the middle
        let middle = Math.floor(arr.length / 2);

        // keep first part of the array
        let res1 = arr.slice(0, middle);

        // keep second part of the array
        let res2 = arr.slice(middle);


        return mergeArrays(mergeSort(res1), mergeSort(res2));
    }

    /**
     * Merge 2 arrays
     * @param arr1
     * @param arr2
     * @returns {*[]}
     */
    function mergeArrays(arr1, arr2) {
        let result = [];

        // loop until longer array
        while (arr1.length && arr2.length) {
            let min = null;
            // compare first elements of each array
            if (arr1[0] < arr2[0]) {
                min = arr1.shift();
            } else {
                min = arr2.shift();
            }
            // keep the smallest element
            result.push(min);
        }

        // concatenate with longest array
        if (arr1.length) {
            result = result.concat(arr1);
        } else (
            result = result.concat(arr2)
        );

        return result;
    }

    const arrayResponse = mergeSort([2, 3, 8, 4, 12, 7]);

    res.render('answer', { arrayResponse, stringResponse: '', objectResponse: {} });
});

module.exports = router;
