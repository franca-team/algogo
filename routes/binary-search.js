var express = require('express');
var router = express.Router();

router.get('/', function (req, res, next) {

    function factorial(num) {
        if (num === 1) {
            return num;
        } else {
            return num * factorial(num - 1);
        }
    }

    function binarySearch(numArray, key) {
        numArray.sort((a, b) => a - b);
        const middleIndex = Math.floor(numArray.length / 2);

        if (key === numArray[middleIndex]) {
            return true;
        } else if (numArray[middleIndex] > key && numArray.length > 1) {
            return binarySearch(numArray.splice(0, middleIndex), key);

        } else if (numArray[middleIndex] < key && numArray.length > 1) {
            return binarySearch(numArray.splice(middleIndex, numArray.length), key);

        } else {
            return false;
        }

    }

    // const stringResponse = factorial(4).toString();
    // => 24

    const stringResponse = binarySearch([45, 78, 66, 21, 31, 26, 42], 26);

    res.render('answer', { arrayResponse: [], stringResponse, objectResponse: {} });
});

module.exports = router;
