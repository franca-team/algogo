var express = require('express');
var router = express.Router();

router.get('/', function (req, res, next) {

    /**
     *
     * @param string
     * @returns {string}
     */
    function isPalindromeWithRegex(string) {
        let resp = 'Not a palindrome';
        string = string.toLowerCase();
        const letters = string.split('').filter((l) => l.trim().length > 0 && l.match('\[a-z]'));

        if (letters.join() === letters.reverse().join()) {
            resp = 'palindrome!';
        }
        return resp;
    }

    function isPalindrome(string) {
        string = string.toLowerCase();
        var charactersArr = string.split('');
        var validCharacters = 'abcdefghijklmnopqrstuvwxyz'.split('');

        var lettersArr = [];
        charactersArr.forEach(char => {
            if (validCharacters.indexOf(char) > -1) lettersArr.push(char);
        });

        return lettersArr.join('') === lettersArr.reverse().join('');
    }

    const response = isPalindromeWithRegex('Madam, I\'m Adam');

    res.render('answer', { arrayResponse: [], stringResponse: response });
});

module.exports = router;


