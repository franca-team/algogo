var express = require('express');
var router = express.Router();

router.get('/', function (req, res, next) {
    function getMean(array) {
        let sum = 0;
        array.forEach((nb) => {
            sum += nb;
        });
        return sum / array.length;
    }

    function getMedian(array) {
        let median;
        //sort to higher
        array.sort((a, b) => a - b);

        if (array.length % 2 !== 0) {
            median = array[Math.floor(array.length / 2)];
        } else {
            const middA = array[array.length / 2 - 1];
            const middB = array[array.length / 2];
            median = (middA + middB) / 2;
        }
        return median;
    }

    /**
     * numbers appear most in array
     * @param array
     */
    function getMode(array) {
        let obj = {};
        let maxFrequency = 0;
        let modes = [];

        array.forEach((elm) => {
            if (!obj[elm]) {
                obj[elm] = 0;
            }
            obj[elm]++;
        });

        for (let num in obj) {
            if (obj[num] > maxFrequency) {
                modes = [num];
                maxFrequency = obj[num];
            } else if (obj[num] === maxFrequency) {
                modes.push(num);
            }
        }

        // case all same redundunce
        if (modes.length === Object.keys(obj).length) {
            modes = [];
        }

        return modes;
    }

    function meanMediumMode(array) {
        return {
            mean: getMean(array),
            median: getMedian(array),
            mode: getMode(array)
        };
    }

    const objectResponse = meanMediumMode([9, 10, 23, 10, 23, 9]);
    console.log('==== objectResponse ==== ', objectResponse);

    res.render('answer', { arrayResponse: [], stringResponse: '', objectResponse });
});

module.exports = router;
