const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const livereload = require('livereload');
const connectLivereload = require('connect-livereload');

const indexRouter = require('./routes/index');
const fizzBuzzRouter = require('./routes/fizz-buzz');
const harmlessBuzzRouter = require('./routes/harmless-randsome');
const isPalindromeRouter = require('./routes/is-palindrome');
const caesarCipherRouter = require('./routes/caesar-cipher');
const reverseWordsRouter = require('./routes/reverse');
const meanMedianModeRouter = require('./routes/mean-median-mode');
const twoSumRouter = require('./routes/two-sum');
const binarySearchRouter = require('./routes/binary-search');
const fibonaccihRouter = require('./routes/fibonacci');
const primeNumberhRouter = require('./routes/prime-number');
const bubbleSorthRouter = require('./routes/bubble-sort');
const mergeSorthRouter = require('./routes/merge-sort');

const app = express();

let liveReloadServer = livereload.createServer();
liveReloadServer.watch(path.join(__dirname, 'public'));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(connectLivereload());

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/fizz-buzz', fizzBuzzRouter);
app.use('/harmless-randsome', harmlessBuzzRouter);
app.use('/is-palindrome', isPalindromeRouter);
app.use('/caesar-cipher', caesarCipherRouter);
app.use('/reverse-words', reverseWordsRouter);
app.use('/mean-median-mode', meanMedianModeRouter);
app.use('/two-sum', twoSumRouter);
app.use('/binary-search', binarySearchRouter);
app.use('/fibonacci', fibonaccihRouter);
app.use('/prime-number', primeNumberhRouter);
app.use('/bubble-sort', bubbleSorthRouter);
app.use('/merge-sort', mergeSorthRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

liveReloadServer.server.once('connection', () => {
    setTimeout(() => {
        liveReloadServer.refresh('/');
    }, 100);
});

module.exports = app;
